#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br
# 2019
#

# vars
OPECV_VERSION="3.4.4"
CWD=$(pwd)


# update system
clear;
echo "Update system...";
echo;
sudo apt update;
sudo apt upgrade -y;
sudo apt -f install;

# dependecies
clear;
echo "Install dependencies...";
echo;
sudo apt -y install build-essential checkinstall cmake git gfortran pkg-config yasm htop;
sudo apt -y install libjpeg-dev libjasper-dev libpng12-dev libtiff5-dev libst-dev libdc1394-22-dev;
sudo apt -y install libavcodec-dev libavformat-dev libswscale-dev libxine2-dev p7zip-full ranger;
sudo apt -y install libv4l-dev libgstreamer0.10-dev libgtk-3-dev libtbb-dev qt5-default libavresample-dev;
sudo apt -y install libatlas-base-dev libmp3lame-dev libtheora-dev libvorbis-dev libxvidcore-dev;
sudo apt -y install libx264-dev libopencore-amrnb-dev libopencore-amrwb-dev libleptonica-dev;
sudo apt -y install x264 v4l-utils libprotobuf-dev protobuf-compiler libgoogle-glog-dev libqt-4-dev;
sudo apt -y install libgflags-dev libgphoto2-dev libeigen3-dev libarmadillo-dev libhdf5-dev doxygen;
sudo apt -y install tesseract-progs libtesseract-dev gogoprotobuf ccache distcc libvtk-dicom-dev;

# python
clear;
echo "Install python dependencies...";
echo;
sudo apt -y install python3-dev python3-pip python3-testresources python3-numpy python3-matplotlib;
sudo apt -y install python-dev python-numpy virtualenv virtualenvwrapper libceres-dev;

