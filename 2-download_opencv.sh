#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2019
#

OPENCV_VERSION="3.4.4"
CWD=$(pwd)


# download opencv
clear;
echo "Download opencv-$OPENCV_VERSION...";
echo;
wget -O opencv.zip https://github.com/Itseez/opencv/archive/$OPENCV_VERSION.zip

# download opencv_contrib
clear;
echo "Download opencv_contrib-$OPENCV_VERSION...";
echo;
wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/$OPENCV_VERSION.zip

# unzip opencv
clear;
7z x opencv.zip;
mkdir src;
mv opencv.zip src/;

# unzip opencv_contrib
clear;
7z x opencv_contrib.zip;
mv opencv_contrib.zip src/;

# create buil folder to cmake
clear;
mkdir opencv-$OPENCV_VERSION/build;
cp 3-cmake.sh opencv-$OPENCV_VERSION/build/;
cp 4-make.sh opencv-$OPENCV_VERSION/build/;

