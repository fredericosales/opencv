#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
#

OPENCV_VERSION="3.4.4"
CWD=$(pwd)

cmake -D CMAKE_BUILD_TYPE=RELEASE \
	  -D CMAKE_INSTALL_PREF=/usr/local \
	  -D INSTALL_C_EXAMPLES=ON \
	  -D INSTALL_PYTHON_EXAMPLES=ON \
	  -D ENABLE_PRECOMPILED_HEADERS=OFF \
	  -D WITH_VTK=ON \
	  -D BUILD_DOCS=ON \
	  -D BUILD_SHARED_LIBS=ON \
	  -D BUILD_NEW_PYTHON_SUPPORT=ON \
	  -D BUILD_opencv_python3=ON \
	  -D BUILD_opencv_python2=ON \
	  -D HAVE_opencv_python3=ON \
	  -D WITH_TBB=ON \
	  -D WITH_V4L=ON \
	  -D WITH_QT=ON \
	  -D WITH_OPENGL=ON \
	  -D OPENCV_EXTRA_MODULES_PATH=$HOME/bin/opencv/opencv_contrib-$OPENCV_VERSION/modules \
	  -D BUILD_EXAMPLES=ON \
	  ..

