#!/bin/bash
#
# Frederico Sales 
# <frederico.sales@engenharia.ufjf.br>
# 2019
#

# make
clear;
echo "Make...";
echo;
make -j$(nproc);


# make install
echo "Make install...";
echo;
sudo make install;
sudo ldconfig;
